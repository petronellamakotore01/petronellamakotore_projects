package zw.co.afrosoft.usermanagmnt.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import zw.co.afrosoft.usermanagmnt.Students;

import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@Data
@Builder
public class StudentsDto{
    private  String studentId;
    private  String firstName;
    private String lastName;
    private LocalDateTime dob;


    public StudentsDto(String studentId, String firstName, String lastName, LocalDateTime dob) {
        this.studentId = studentId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
    }

    public static StudentsDto of(Students students) {
        return new StudentsDto(
              students.getStudentId(),
                students.getFirstName(),
                students.getLastName(),
                students.getDob());
    }

    public static List<StudentsDto> of(List<Students> students) {
        return students.stream().map(StudentsDto::of)
                .collect(Collectors.toList());
    }
}