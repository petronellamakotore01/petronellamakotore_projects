package zw.co.afrosoft.usermanagmnt.util;
import java.io.Serializable;

import static zw.co.afrosoft.usermanagmnt.utils.Constants.FAILURE_INT_VALUE;
import static zw.co.afrosoft.usermanagmnt.utils.Constants.SUCCESS_INT_VALUE;


public class Response <T> implements Serializable {
    private int statusCode;
    private boolean success;
    private String message;
    private T result;

    public Response<T> buildSuccessResponse(String message, final T result) {
        this.statusCode = SUCCESS_INT_VALUE;
        this.success = true;
        this.message = message;
        this.result = result;
        return this;
    }

    public Response<T> buildErrorResponse(String message) {
        this.statusCode = FAILURE_INT_VALUE;
        this.success = false;
        this.message = message;
        this.result = null;
        return this;
    }

    public Response<T> buildErrorResponse(String message, final T result) {
        this.statusCode = FAILURE_INT_VALUE;
        this.success = false;
        this.message = message;
        this.result = result;
        return this;
    }

    public Response() {
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }


}
