package zw.co.afrosoft.usermanagmnt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zw.co.afrosoft.usermanagmnt.dto.StudentsDto;
import zw.co.afrosoft.usermanagmnt.student.StudentService;
import zw.co.afrosoft.usermanagmnt.student.dto.StudentDetails;
import zw.co.afrosoft.usermanagmnt.util.Response;
import zw.co.afrosoft.usermanagmnt.utils.Constants;

import java.util.List;
import java.util.Set;


@Slf4j
@RestController
@RequestMapping("/students")
public class StudentsRestController {
    private  final StudentService studentService;

    public StudentsRestController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping
    public Response<StudentsDto>registerStudent(@RequestBody StudentDetails studentDetails){
        Students student = studentService.registerStudent(studentDetails);
        return new Response<StudentsDto>().buildSuccessResponse(Constants.SUCCESS, StudentsDto.of(student));
    }

    @PostMapping("/list")
    public Response<List<StudentsDto>> registerStudent(@RequestBody Set<StudentDetails> studentDetailsList){
        List<Students> students = studentService.registerStudents(studentDetailsList);
        return new Response<List<StudentsDto>>().buildSuccessResponse(Constants.SUCCESS, StudentsDto.of(students));
    }


}
