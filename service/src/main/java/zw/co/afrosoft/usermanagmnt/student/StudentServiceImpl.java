package zw.co.afrosoft.usermanagmnt.student;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import zw.co.afrosoft.usermanagmnt.Students;
import zw.co.afrosoft.usermanagmnt.StudentsRepository;
import zw.co.afrosoft.usermanagmnt.student.dto.StudentDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
public class StudentServiceImpl implements StudentService{

    private final StudentsRepository studentsRepository;

    public StudentServiceImpl(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @Override
    public Students registerStudent(StudentDetails studentDetails) {
        Students students = Students.builder()
                .studentId(studentDetails.getStudentId())
                .firstName(studentDetails.getFirstName())
                .lastName(studentDetails.getLastName())
                .dob(studentDetails.getDob())
                .build();
        return studentsRepository.save(students);
    }


    public List<Students> registerStudents(Set<StudentDetails> studentDetailsList) {
        ArrayList<Students> students = new ArrayList<>();
        studentDetailsList.forEach(studentDetails -> {
            Students student = Students.builder()
                    .studentId(studentDetails.getStudentId())
                    .firstName(studentDetails.getFirstName())
                    .lastName(studentDetails.getLastName())
                    .dob(studentDetails.getDob())
                    .build();
            students.add(student);
        });
        return studentsRepository.saveAll(students);
    }
    

}
