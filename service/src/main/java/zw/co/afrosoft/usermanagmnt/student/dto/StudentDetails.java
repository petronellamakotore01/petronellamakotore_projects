package zw.co.afrosoft.usermanagmnt.student.dto;

import lombok.*;

import javax.persistence.Column;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class StudentDetails {
    private  String studentId;
    private  String firstName;
    private String lastName;
    private LocalDateTime dob;
}
