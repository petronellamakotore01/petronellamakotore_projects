package zw.co.afrosoft.usermanagmnt.student;


import zw.co.afrosoft.usermanagmnt.Students;
import zw.co.afrosoft.usermanagmnt.student.dto.StudentDetails;

import java.util.List;
import java.util.Set;

public interface StudentService {
    Students registerStudent(StudentDetails studentDetails);

  List<Students> registerStudents(Set<StudentDetails> studentDetailsList);

}
