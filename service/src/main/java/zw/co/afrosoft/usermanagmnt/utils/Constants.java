package zw.co.afrosoft.usermanagmnt.utils;

public interface Constants {
    int FAILURE_INT_VALUE = 400;
    int SUCCESS_INT_VALUE = 200;
    String SUCCESS = "Success";
    String FAILED = "Failure";
    String DELETED_COUNTRY = "Country Deleted Successfully";

}
