package zw.co.afrosoft.usermanagmnt;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@Table
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Students implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;

    @Column
    private  String studentId;
    @Column
    private  String firstName;
    @Column
    private String lastName;
    @Column
    private LocalDateTime dob;



}
